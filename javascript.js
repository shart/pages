const main = async () => {
  console.log("alamo");
  const map = L.map("map").setView([51.505, -0.09], 3);

  L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 19,
    attribution:
      '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  }).addTo(map);

  const coords = await (await fetch("/coords.json")).json();
  coords.forEach((x) => {
    const countryIcon = L.icon({
      iconUrl: `https://static.ylilauta.org/img/flags/${x.code.toLowerCase()}.png`,
      //`/static/img/${x.code.toLowerCase()}.png`,
      //iconSize: [32, 32], // size of the icon
      iconAnchor: [16, 16], // point of the icon which will correspond to marker's location
      popupAnchor: [16, 0], // point from which the popup should open relative to the iconAnchor
    });
    L.marker([x.lat, x.lon], { icon: countryIcon })
      .addTo(map)
      .bindPopup(x.name);
  });
};
main();
